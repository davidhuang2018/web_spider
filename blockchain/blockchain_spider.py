import requests
from lxml import etree
from retrying import retry
import psycopg2
import pandas as pd
import logging as log
import json
from datetime import datetime,timedelta
import re
import redis
import time

addrs_redis = redis.Redis(host='172.19.2.65',port=6379,password='aVeryLongPassword12345678',decode_responses=True,db=13)


###########################################
# logger配置
# BASE_DIR = os.path.dirname(__file__)
# LOG_PATH = BASE_DIR + '/'
LOG_FILENAME = 'blockchain.log'

# 创建一个logger
logger = log.getLogger('my_log')
logger.setLevel(log.DEBUG)

# 创建一个handler，用于写入日志文件
output_file = log.FileHandler(LOG_FILENAME)
output_file.setLevel(log.DEBUG)

# 再创建一个handler，用于输出到控制台
console_info = log.StreamHandler()
console_info.setLevel(log.DEBUG)

# 定义handler的输出格式
formatter = log.Formatter('[%(levelname)s] %(message)s')
output_file.setFormatter(formatter)
console_info.setFormatter(formatter)

# 给logger添加handler
logger.addHandler(output_file)
logger.addHandler(console_info)


headers = {
        "User-Agent": 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36',
    "cookie":"__cfduid=d7545c29724eb14c8869387c279e1d4491527921276; _ga=GA1.2.2096265966.1527927284; _gid=GA1.2.1054171919.1528703575; _gat=1"
    }

block_url = 'https://blockchain.info'


def fmt(info):
    info_format = "'" + str(info) + "'"
    return info_format

def fmt_f(info):
    info_format =  float(info)
    return info_format

host_url = 'https://blockchain.info/blocks/'

#获取当前日期下所有链信息
@retry(stop_max_attempt_number=4)
def get_block_info():
    id_list = ['1528874175840','1528787775840','1528701375840','1528614975840','1528528575840']
    address_list = []
    for i in id_list:
        url = host_url + i
        print(url)
        response = requests.get(url,headers=headers,timeout=5)
        decontent = response.content.decode()
        html = etree.HTML(decontent)
        tr_list = html.xpath("//div[contains(@class,'container pt-100')]/table//tr")
        for tr in tr_list:
            addr = tr.xpath('./td[3]/a/@href')[0] if len(tr.xpath('./td[3]/a/@href')) > 0 else None
            if addr is not None:
                print(addr)
                address_list.append(addr)
    return address_list

@retry(stop_max_attempt_number=4)
def get_address_info():
    conn = psycopg2.connect(database="blockchain", user="user1", password="pass1", host="172.19.2.65", port="5432")
    cur = conn.cursor()
    addrs = get_block_info()
    print(addrs)
    for i in range(len(addrs)):
        url = block_url + addrs[i]
        print(url)
        response = requests.get(url,headers=headers,timeout=30)
        decontent = response.content.decode()
        html = etree.HTML(decontent)
        div_list = html.xpath("//div[contains(@class,'container pt-100')]/div[@class='row']/following-sibling::div/div")
        now = datetime.now()
        for div in div_list:
            exchange = 'unconfirmed'
            symbol = 'BTC'
            contract_type = 'blockchain'
            meta_data = {}
            frm_info = div.xpath('./table//tr[2]/td[1]//text()') if len(div.xpath('./table//tr[2]/td[1]//text()')) > 0 else None
            meta_data['frm'] = frm_info
            to_info = div.xpath('./table//tr[2]/td[3]//text()') if len(div.xpath('./table//tr[2]/td[3]//text()')) > 2 else None
            if to_info is not None:
                to_info = [t for t in to_info if t != ' ']
                meta_data['to'] = {to_info[t]: to_info[t+1] for t in range(0, len(to_info)-1, 2)}
            amount = div.xpath('./div/button/span/text()')[0] if len(div.xpath('./div/button/span/text()')) > 0 else None
            meta_data['amount'] = amount
            trade_date = div.xpath('./table//tr[1]//text()')[-1] if len(div.xpath('./table//tr[1]//text()')) > 0 else None
            meta_data['date'] = trade_date
            block = div.xpath('//h1/text()')[0] if len(div.xpath('//h1/text()')) > 0 else None
            meta_data = json.dumps(meta_data)
            sql = "INSERT INTO public.trade(data_type, exchange, symbol, contract_type, meta_data, updated_time)VALUES (%s, %s, %s, %s, %s, %s);" % (
            fmt(block), fmt(exchange), fmt(symbol), fmt(contract_type), fmt(meta_data), fmt(now))
            print(sql)
            cur.execute(sql)
        conn.commit()

@retry(stop_max_attempt_number=4)
def get_acounturl_save():
    addrs = get_block_info()
    addrs_redis.delete('urls')
    for i in range(len(addrs)):
        url = block_url + addrs[i]
        print(url)
        response = requests.get(url,headers=headers,timeout=20)
        decontent = response.content.decode()
        html = etree.HTML(decontent)
        div_list = html.xpath("//div[contains(@class,'container pt-100')]/div[@class='row']/following-sibling::div/div")
        for div in div_list:
            frm_info = div.xpath('./table//tr[2]/td[1]/a/@href') if len(div.xpath('./table//tr[2]/td[1]/a/@href')) > 0 else None
            to_info = div.xpath('./table//tr[2]/td[3]/a/@href') if len(div.xpath('./table//tr[2]/td[3]/a/@href')) > 0 else None
            if frm_info is not None and to_info is not None:
                acounts =frm_info+to_info
                for a in acounts:
                    acount_url = block_url + a
                    addrs_redis.sadd('urls',acount_url)
                    print('save urls')

    get_acount_info()

@retry(stop_max_attempt_number=4)
def get_acount_info():
    conn = psycopg2.connect(database="blockchain", user="user1", password="pass1", host="172.19.2.65", port="5432")
    cur = conn.cursor()
    now = datetime.now()
    while True:
        Flag = True
        url = addrs_redis.spop('urls')
        if url is None:
            break
        print(url)
        addr = re.split('/', url)[-1]
        response = requests.get(url, headers=headers, timeout=30)
        decontent = response.content.decode()
        html = etree.HTML(decontent)
        print(html)
        balance = html.xpath("//div[contains(@class,'col-md-4 col-xs-12')]/table//tr[4]/td[2]//text()")[0] if len(html.xpath("//div[contains(@class,'col-md-4 col-xs-12')]/table//tr[4]/td[2]//text()")) > 0 else 0
        Received = html.xpath("//div[contains(@class,'col-md-4 col-xs-12')]/table//tr[3]/td[2]//text()")[0] if len(html.xpath("//div[contains(@class,'col-md-4 col-xs-12')]/table//tr[3]/td[2]//text()")) > 0 else 0
        try:
            balance = balance.replace(' BTC','').replace(',','')
            Received = Received.replace(' BTC', '').replace(',', '')
            balance = float(balance)
            Received = float(Received)
        except Exception as e:
            print(e)
        if balance > 1000:
            print(balance)
            div_list = html.xpath(
                "//div[contains(@class,'container pt-100')]/div[@class='row']/following-sibling::div/div/div[contains(@class,'txdiv')]")
            for div in div_list:
                trade_date = div.xpath('./table//tr[1]/th/span/text()')[0] if len(div.xpath('./table//tr[1]//text()')) > 0 else None
                if trade_date is not None:
                    trade_date = datetime.strptime(trade_date,"%Y-%m-%d %H:%M:%S")
                    if trade_date < (now - timedelta(days=3)):
                        Flag = False
                        break
                amount = div.xpath('./div/button/span/text()')[0] if len(div.xpath('.//button/span/text()')) > 0 else '0'
                try:
                    amount = amount.replace(' BTC','').replace(',','')
                    amount = float(amount)
                except Exception as e:
                    print(e)
                sql = """INSERT INTO public.acount(addr, balance, received, net, trade_date, updated_time)VALUES (%s, %s, %s, %s, %s, %s);""" % (
        fmt(addr),fmt_f(balance), fmt_f(Received), fmt_f(amount), fmt(trade_date), fmt(now))
                print(sql)
                cur.execute(sql)
                conn.commit()
            while Flag:
                next_available = html.xpath("//li[contains(@class,'next')]/@class")[0] if len(html.xpath("//li[contains(@class,'next')]/@class"))>0 else None
                print(next_available)
                if next_available != 'next disabled':
                    next_page = html.xpath("//a[contains(text(),'Next →')]/@href")[0] if len(html.xpath("//a[contains(text(),'Next →')]/@href")) > 0 else None
                    acount_next_url = url + next_page
                    response = requests.get(acount_next_url, headers=headers, timeout=30)
                    decontent = response.content.decode()
                    html = etree.HTML(decontent)
                    div_list = html.xpath(
                        "//div[contains(@class,'container pt-100')]/div[@class='row']/following-sibling::div/div/div[contains(@class,'txdiv')]")
                    for div in div_list:
                        trade_date = div.xpath('./table//tr[1]/th/span/text()')[0] if len(
                            div.xpath('./table//tr[1]//text()')) > 0 else None
                        amount = div.xpath('./div/button/span/text()')[0] if len(
                            div.xpath('.//button/span/text()')) > 0 else '0'
                        try:
                            amount = amount.replace(' BTC', '').replace(',', '')
                            amount = float(amount)
                        except Exception as e:
                            print(e)
                        sql = """INSERT INTO public.acount(addr, balance, received, net, trade_date, updated_time)VALUES (%s, %s, %s, %s, %s, %s);""" % (
                            fmt(addr), fmt_f(balance), fmt_f(Received), fmt_f(amount), fmt(trade_date), fmt(now))
                        print(sql)
                        cur.execute(sql)
                        conn.commit()
                else:
                    print('final page')
                    break





def run():
    # // a[contains(text(), '<< Previous')] / @ href
    pass

if __name__ == '__main__':
    get_acounturl_save()
    get_acount_info()