import requests
from lxml import etree
from retrying import retry
import psycopg2
import json
from datetime import datetime,timedelta
import re
import redis
import numpy as np
import pandas as pd

headers = {
        "User-Agent": 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36'
    }

addrs_redis = redis.Redis(host='172.19.2.65',port=6379,password='aVeryLongPassword12345678',decode_responses=True,db=9)

block_url = 'https://gastracker.io/block/'
host_url = 'https://api.gastracker.io/v1'

def fmt(info):
    info_format = "'" + str(info) + "'"
    return info_format


#获取当前日期下所有链信息
@retry(stop_max_attempt_number=4)
def get_block_info():
    url = "https://api.gastracker.io/v1/blocks/latest"
    response = requests.get(url)
    data = response.content.decode()
    dic_data = json.loads(data)
    address_list = [item['hash'] for item in dic_data['items']]
    return address_list

@retry(stop_max_attempt_number=4)
def get_acounturl_save():
    addrs_redis.delete('urls')
    for i in range(5994362,5993000,-1):
        url = block_url + str(i)
        print(url)
        response = requests.get(url,headers=headers,timeout=20)
        decontent = response.content.decode()
        html = etree.HTML(decontent)
        frm_info = html.xpath("//div[contains(@class,'col-sm-12')]/table/tbody/tr/td[3]/a/@href") if len(html.xpath("//div[contains(@class,'col-sm-12')]/table/tbody/tr/td[3]/a/@href")) > 0 else None
        to_info = html.xpath("//div[contains(@class,'col-sm-12')]/table/tbody/tr/td[4]/a/@href") if len(html.xpath("//div[contains(@class,'col-sm-12')]/table/tbody/tr/td[4]/a/@href")) > 0 else None
        if frm_info is not None and to_info is not None:
            acounts =frm_info+to_info
            for a in acounts:
                acount_url = host_url + a
                addr = re.split('/', acount_url)[-1]
                acount_url = acount_url + '/operations'
                if len(addr) == 42:
                    addrs_redis.sadd('urls',acount_url)
                    print(acount_url)
                    print('save urls')


@retry(stop_max_attempt_number=4)
def get_acount_info():
    conn = psycopg2.connect(database="blockchain", user="user1", password="pass1", host="172.19.2.65", port="5432")
    cur = conn.cursor()
    now = datetime.now()
    while True:
        url = addrs_redis.spop('urls')
        if url is None:
            break
        addr = re.split('/', url)[-2]
        response = requests.get(url)
        data = response.content.decode()
        dic_data = json.loads(data)
        trade_date_list = [item['timestamp'] for item in dic_data['items']]
        amount_list = [float(item['value']['ether']) if item['to'].upper() == addr.upper() else float(item['value']['ether']) * (-1) for item in dic_data['items']]
        if len(trade_date_list) == len(amount_list):
            for i in range(len(amount_list)):
                if trade_date_list[i] is not None:
                    date = datetime.strptime(trade_date_list[i],"%Y-%m-%dT%H:%M:%S")
                    if date < (now - timedelta(days=3)):
                        continue
                    sql = """INSERT INTO public.gastracker(addr, amount, trade_date, updated_time)VALUES (%s, %s, %s, %s);""" % (
            fmt(addr),amount_list[i], fmt(date), fmt(now))
                    print(sql)
                    cur.execute(sql)
                    conn.commit()
                    print('ok')

def pull_data():
    conn = psycopg2.connect(database="blockchain", user="user1", password="pass1", host="172.19.2.65", port="5432")
    cur = conn.cursor()
    query_sql = """SELECT net, trade_date, addr, updated_time FROM public.acount;"""
    cur.execute(query_sql)
    db_data = cur.fetchall()
    amount = [float(data[0]) for data in db_data]
    trade_date = [datetime.strptime(data[1],"%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d %H:00:00") for data in db_data]
    addr = [data[2] for data in db_data]
    gas_df = pd.DataFrame({'addr':addr,'trade_date':trade_date,'amount':amount})
    print(len(gas_df))
    gas = gas_df.groupby('trade_date').sum()
    print(gas)
    # s = np.sum(amount)


if __name__ == '__main__':
    pull_data()
