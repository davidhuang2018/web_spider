import requests
from lxml import etree
from retrying import retry
import psycopg2
from datetime import datetime,timedelta
import re
import redis
import time

headers = {
        "User-Agent": 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36'
    }

addrs_redis = redis.Redis(host='172.19.2.65',port=6379,password='aVeryLongPassword12345678',decode_responses=True,db=10)

block_url = 'http://explorer.litecoin.net'

def fmt(info):
    info_format = "'" + str(info) + "'"
    return info_format

def fmt_f(info):
    info_format =  float(info)
    return info_format

def fmt_re(str):
    reg = re.compile(r'[(](.*?)[)]', re.S)
    fmt_str = re.findall(reg, str)
    amount = 0
    try:
        if len(fmt_str) > 0:
            amount = float(fmt_str[0]) * (-1)
        elif len(fmt_str) == 0:
            amount = float(str)
    except Exception as e:
        print(e)
    return amount

#获取当前日期下所有链信息
@retry(stop_max_attempt_number=4)
def get_block_info():
    url = 'http://explorer.litecoin.net/chain/Litecoin'
    response = requests.get(url,headers=headers,timeout=5)
    decontent = response.content.decode()
    html = etree.HTML(decontent)
    url_list = html.xpath("//div[contains(@class,'row')]/table//tr/td[1]/a/@href")
    url_list = [url.replace('..','') for url in url_list]
    return url_list

@retry(stop_max_attempt_number=4)
def get_acounturl_save():
    addrs = get_block_info()
    for i in range(len(addrs)):
        url = block_url + addrs[i]
        response = requests.get(url,headers=headers,timeout=20)
        decontent = response.content.decode()
        html = etree.HTML(decontent)
        frm_info = html.xpath("//div[contains(@class,'panel')]/table//tr/td[4]/a/@href") if len(html.xpath("//div[contains(@class,'panel')]/table//tr/td[4]/a/@href")) > 0 else None
        to_info = html.xpath("//div[contains(@class,'panel')]/table//tr/td[5]/a/@href") if len(html.xpath("//div[contains(@class,'panel')]/table//tr/td[5]/a/@href")) > 0 else None
        if frm_info is not None and to_info is not None:
            acounts =frm_info+to_info
            acounts = [url.replace('..','') for url in acounts]
            for a in acounts:
                acount_url = block_url + a
                addr = re.split('/', acount_url)[-1]
                print(acount_url)
                if len(addr) == 34:
                    addrs_redis.sadd('urls',acount_url)
                    print('save urls')


@retry(stop_max_attempt_number=4)
def get_acount_info():
    conn = psycopg2.connect(database="blockchain", user="user1", password="pass1", host="172.19.2.65", port="5432")
    cur = conn.cursor()
    now = datetime.now()
    while True:
        url = addrs_redis.spop('urls')
        if url is None:
            break
        print(url)
        addr = re.split('/', url)[-1]
        while True:
            try:
                response = requests.get(url, headers=headers, timeout=30)
                decontent = response.content.decode()
            except Exception as e:
                print(e)
            if len(decontent) > 0:
                print(decontent)
                break
            else:
                time.sleep(10)
        html = etree.HTML(decontent)
        content = html.xpath("//div[contains(@class,'panel')]/p[2]/text()") if len(html.xpath("//div[contains(@class,'panel')]/p[2]/text()")) > 0 else None
        print(content)
        if len(content) > 3:
            try:
                balance = content[0].split(' ')[1]
                received = content[2].split(' ')[1]
                balance = float(balance)
                received = float(received)
            except Exception as e:
                print(e)
            if balance > 100:
                trade_date_list = html.xpath("//div[contains(@class,'panel')]/table//tr/td[3]/text()") if len(html.xpath("//div[contains(@class,'panel')]/table//tr/td[3]/text()")) > 0 else None
                amount_list = html.xpath("//div[contains(@class,'panel')]/table//tr/td[4]/text()") if len(html.xpath("//div[contains(@class,'panel')]/table//tr/td[4]/text()")) > 0 else None
                amount_list = [fmt_re(a) for a in amount_list]
                if len(trade_date_list) == len(amount_list):
                    for i in range(len(amount_list)):
                        if trade_date_list[i] is not None:
                            date = datetime.strptime(trade_date_list[i],"%Y-%m-%d %H:%M:%S")
                            if date < (now - timedelta(days=3)):
                                continue
                            sql = """INSERT INTO public.litecoin(addr, balance, received, net, trade_date, updated_time)VALUES (%s, %s, %s, %s, %s, %s);""" % (
                    fmt(addr),balance,received, fmt_f(amount_list[i]), fmt(date), fmt(now))
                            print(sql)
                            cur.execute(sql)
                            conn.commit()
                            print('ok')
        else:
            print()
            break


if __name__ == '__main__':
    get_acounturl_save()
    get_acount_info()